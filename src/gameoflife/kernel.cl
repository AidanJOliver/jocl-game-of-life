int Modulo(float a, float b)
{
    return(int) a - floor(a/b) * b;
}

__kernel void GOLKernel(__global const int *input, __global int *output)
{
    // Array has been reduced to one dimension
    // Modulo calculations are required to allow the envorinment to "wrap"

    int N = 100;
    int i = get_global_id(0);

    int up = Modulo(i-N, N*N);
    int down = (i+N) % (N*N);
    int left = (i - Modulo(i,N)) + Modulo(i-1, N);
    int right = i - (i % N) + (i+1) % N;

    int upLeft = (up - Modulo(up,N)) + Modulo(up-1, N);
    int downLeft = (down - Modulo(down,N)) + Modulo(down-1 , N);
    int upRight = (up - (up % N)) + ((up+1) % N);
    int downRight = (down - (down % N)) + ((down+1) % N);

    int neighbours = input[upLeft]   + input[up]    + input[upRight] 
                + input[left]                    + input[right] 
                + input[downLeft] + input[down]  + input[downRight];

    int originalValue = input[i];

    switch(neighbours)
    {
        case 2 : output[i] = originalValue; break;
        case 3 : output[i] = 1; break;
        default: output[i] = 0; break;
    };
}

