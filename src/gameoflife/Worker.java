package gameoflife;

class Worker extends Thread
{
    private GameOfLife game;

    public Worker(GameOfLife g)
    {
        this.game = g;
    }

    public void run()
    {
        // Starts running generations in a continuous loop
        game.work();
    }
}