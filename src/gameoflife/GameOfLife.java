package gameoflife;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JFrame;

public class GameOfLife extends JFrame
{
    private static final long serialVersionUID = -2696591779345023587L;
    private LifeEnv environment;
    boolean working = true;

    GameOfLife()
    {
        environment = new LifeEnv();

        setTitle("Game of Life");
        setLayout(new BorderLayout());
        add("Center", environment);
        setPreferredSize(new Dimension(800, 800));

        WindowAdapter w = new WindowAdapter()
        {
            public void windowClosing(WindowEvent we)
            {
                working = false;
                environment.end();
                dispose();
            }
        };

        addWindowListener(w);
        pack();
        setVisible(true);
        init();
    }

    // Run a thread which calls the work method below
    public void init()
    {
        new Worker(this).start();
    }

    // Begins simulation by continuously updating environment
    public void work()
    {
        while (working)
        {
            environment.runOnce();
        }
    }

    public static void main(final String... args)
    {
        new GameOfLife();
    }
}