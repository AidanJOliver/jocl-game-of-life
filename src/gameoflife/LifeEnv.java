package gameoflife;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.swing.JPanel;
import org.jocl.*;
import static org.jocl.CL.*;

class LifeEnv extends JPanel
{
    private static final long serialVersionUID = 1497486671974977008L;

    // UI variables
    private static final int POINT_SIZE = 7;
    private static final Color POINT_COLOR = Color.blue;
    private static final int CANVAS_SIZE = 800;
    private static final int FPS = 50;

    // Width and height of environment
    // (Array dimension size prior to flattening)
    private static final int N = 100;

    // 1D size after flattening the 2D array
    private static final int size = N * N;

    // Relative kernel file location
    private static final String pathToKernel = "./kernel.cl";
    private static final String kernelMethodName = "GOLKernel";

    // 2D matrix used as a template for the initial game state
    private static int[][] current;

    // Matrices used for maintaining and updating game state
    private static int[] flatUpdate;
    private static int[] flatCurrent;

    // OpenCL variables
    private static final int platformIndex = 0;
    private static final long deviceType = CL_DEVICE_TYPE_ALL;
    private static final long[] globalWorkSize = new long[] { size };
    private static final long[] localWorkSize = new long[] { 10 };
    private static final int[] numPlatformsArray = new int[1];
    private static final int[] numDevicesArray = new int[1];
    private static long properties = 0;

    private static cl_kernel kernel;
    private static cl_program program;
    private static cl_platform_id platform;
    private static cl_context_properties contextProperties;
    private static cl_context context;
    private static cl_device_id devices[];
    private static cl_platform_id[] platforms;
    private static cl_mem[] inputMems;
    private static cl_mem[] outputMems;
    private static cl_event[] events;
    private static cl_command_queue[] commandQueues;
    private static int numDevices;
    private static int numPlatforms;
    private static String programSource;

    public LifeEnv()
    {
        // This array is only used for simple setup prior to flattening
        current = new int[N][N];

        // Glider
        current[21][20] = 1;
        current[22][21] = 1;
        current[22][22] = 1;
        current[21][22] = 1;
        current[20][22] = 1;

        // Spaceship
        current[50][50] = 1;
        current[53][50] = 1;
        current[54][51] = 1;
        current[50][52] = 1;
        current[54][52] = 1;
        current[51][53] = 1;
        current[52][53] = 1;
        current[53][53] = 1;
        current[54][53] = 1;

        // UI settings for the JPanel
        setSize(CANVAS_SIZE, CANVAS_SIZE);
        setBackground(Color.white);
        setDoubleBuffered(true); // Prevents flickering
        setOpaque(false); // Ensures image is completely refreshed

        flatCurrent = Arrays.stream(current).flatMapToInt(Arrays::stream)
                .toArray();
        flatUpdate = Arrays.stream(current).flatMapToInt(Arrays::stream)
                .toArray();

        setCLVariables();
    }

    static void setCLVariables()
    {

        CL.setExceptionsEnabled(true);

        // Get platform count
        clGetPlatformIDs(0, null, numPlatformsArray);
        numPlatforms = numPlatformsArray[0];

        // Get platform ID
        platforms = new cl_platform_id[numPlatforms];
        clGetPlatformIDs(platforms.length, platforms, null);
        platform = platforms[platformIndex];

        // Context properties
        contextProperties = new cl_context_properties();
        contextProperties.addProperty(CL_CONTEXT_PLATFORM, platform);

        // Get device count
        clGetDeviceIDs(platform, deviceType, 0, null, numDevicesArray);
        numDevices = numDevicesArray[0];

        // Get device IDs
        devices = new cl_device_id[numDevices];
        clGetDeviceIDs(platform, deviceType, numDevices, devices, null);

        // Print some useful information about the platforms and devices used
        String platformName = getString(platform, CL_PLATFORM_NAME);
        System.out.println("Using platform " + (platformIndex + 1) + " of "
                + numPlatforms + ": " + platformName);
        for (int i = 0; i < numDevices; i++)
        {
            String deviceName = getString(devices[i], CL_DEVICE_NAME);
            System.out.println("Device " + (i + 1) + " of " + numDevices + ": "
                    + deviceName);
        }

        // Create context
        context = clCreateContext(contextProperties, devices.length, devices,
                null, null, null);
        // Attempt to build kernel from specified filepath
        try
        {
            programSource = readStream(pathToKernel);
            program = clCreateProgramWithSource(context, 1,
                    new String[] { programSource }, null, null);
            clBuildProgram(program, 0, null, null, null, null);
            kernel = clCreateKernel(program, kernelMethodName, null);
        } catch (IOException e)
        {
            // File Could not be found
            e.printStackTrace();
        }

        // Initialize variables to hold the memory buffers
        inputMems = new cl_mem[numDevices];
        outputMems = new cl_mem[numDevices];

        // Initialize variables to hold the command queues and events
        commandQueues = new cl_command_queue[numDevices];
        events = new cl_event[numDevices];

    }

    public void runOnce()
    {

        for (int i = 0; i < numDevices; i++)
        {
            // Create command queue
            commandQueues[i] = clCreateCommandQueue(context, devices[i],
                    properties, null);

            // Create the input and output memory buffers
            inputMems[i] = clCreateBuffer(context,
                    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                    Sizeof.cl_float * size, Pointer.to(flatCurrent),
                    null);
            outputMems[i] = clCreateBuffer(context, CL_MEM_READ_WRITE,
                    Sizeof.cl_float * size, null, null);

            // Set Kernel Arguments (arrays to be passed)
            clSetKernelArg(kernel, 0, Sizeof.cl_mem, Pointer.to(inputMems[i]));
            clSetKernelArg(kernel, 1, Sizeof.cl_mem, Pointer.to(outputMems[i]));

            events[i] = new cl_event();

            // Begin calculations on the kernel
            clEnqueueNDRangeKernel(commandQueues[i], kernel, 1, null,
                    globalWorkSize, localWorkSize, 0, null, events[i]);
        }

        // Wait for command queues to complete calculating
        clWaitForEvents(events.length, events);

        // Read the results into "flatUpdate" array
        clEnqueueReadBuffer(commandQueues[0], outputMems[0], CL_TRUE, 0,
                size * Sizeof.cl_float, Pointer.to(flatUpdate), 0,
                null, null);

        // Free up memory to prevent memory leaks
        // Because context and kernel are reused they do not need to be released
        for (int i = 0; i < numDevices; i++)
        {
            clReleaseMemObject(inputMems[i]);
            clReleaseMemObject(outputMems[i]);
            clReleaseEvent(events[i]);
            clReleaseCommandQueue(commandQueues[i]);
        }

        // Update the current game state
        flatCurrent = flatUpdate;

        // Adds a small delay
        limitFPS(FPS);

        repaint();
    }

    // These do not need actually to be freed unless the program is to be
    // expanded beyond the runOnce loop
    void end()
    {
        clReleaseProgram(program);
        clReleaseContext(context);
        clReleaseKernel(kernel);
    }

    static String readStream(final String fName) throws IOException
    {
        final InputStreamReader input = new InputStreamReader(
                LifeEnv.class.getResourceAsStream(fName));
        final BufferedReader reader = new BufferedReader(input);
        final String text = reader.lines().parallel()
                .collect(Collectors.joining("\n"));
        reader.close();
        return text;
    }

    private static String getString(cl_device_id device, int paramName)
    {
        long size[] = new long[1];
        clGetDeviceInfo(device, paramName, 0, null, size);
        byte buffer[] = new byte[(int) size[0]];
        clGetDeviceInfo(device, paramName, buffer.length, Pointer.to(buffer),
                null);
        return new String(buffer, 0, buffer.length - 1);
    }

    private static String getString(cl_platform_id platform, int paramName)
    {
        long size[] = new long[1];
        clGetPlatformInfo(platform, paramName, 0, null, size);
        byte buffer[] = new byte[(int) size[0]];
        clGetPlatformInfo(platform, paramName, buffer.length,
                Pointer.to(buffer), null);
        return new String(buffer, 0, buffer.length - 1);
    }

    // Limit the theoretical maximum of updates per second
    public void limitFPS(int FPS)
    {
        // Required delay in milliseconds for target FPS
        int calculatedDelay = 1000 / FPS;
        try
        {
            Thread.sleep(calculatedDelay);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    // Draw the living points where value = 1
    public void paint(Graphics g)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                int flatindex = i * 100 + j;
                int value = flatCurrent[flatindex];
                if (value == 1)
                {
                    drawPoint(i, j, 1, g);
                }
            }
        }
    }

    private void drawPoint(int x, int y, int v, Graphics g)
    {
        Dimension d = (getSize());
        int mx = d.width * x / N;
        int my = d.height * y / N;
        g.setColor(POINT_COLOR);
        g.fillRect(mx, my, POINT_SIZE, POINT_SIZE);
    }
}